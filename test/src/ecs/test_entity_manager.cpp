#include "gtest/gtest.h"
#include "kine/ecs/entity.h"
#include "kine/ecs/entity_manager.h"

using namespace kine::ecs;

TEST(ecs_EntityManager, initial) {
  EntityManager em;
  auto entities = em.getEntities();
  ASSERT_EQ(entities.size(), 0);
}

TEST(ecs_EntityManager, create_delete_entity) {
  EntityManager em;

  auto& entities = em.getEntities();
  ASSERT_EQ(entities.size(), 0);

  auto e = em.createEntity();
  ASSERT_EQ(entities.size(), 1);

  em.destroyEntity(e);
  ASSERT_EQ(entities.size(), 0);
}

TEST(ecs_EntityManager, create_delete_3_entity) {
  EntityManager em;

  auto& entities = em.getEntities();
  ASSERT_EQ(entities.size(), 0);

  auto e1 = em.createEntity();
  auto e2 = em.createEntity();
  auto e3 = em.createEntity();
  ASSERT_EQ(entities.size(), 3);

  em.destroyEntity(e1);
  em.destroyEntity(e2);
  em.destroyEntity(e3);
  ASSERT_EQ(entities.size(), 0);
}

TEST(ecs_EntityManager, create_delete_3_entity_reverse_order) {
  EntityManager em;

  auto& entities = em.getEntities();
  ASSERT_EQ(entities.size(), 0);

  auto e1 = em.createEntity();
  auto e2 = em.createEntity();
  auto e3 = em.createEntity();
  ASSERT_EQ(entities.size(), 3);

  em.destroyEntity(e1);
  em.destroyEntity(e2);
  em.destroyEntity(e3);

  ASSERT_EQ(entities.size(), 0);
}
