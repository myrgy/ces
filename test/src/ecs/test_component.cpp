#include "gtest/gtest.h"
#include "kine/ecs/component.h"

namespace {
    class TestComponent : public kine::ecs::Component {
    public:
        TestComponent() {}
    };
}

TEST(ecs_Component, empty) {
    TestComponent c;
    ASSERT_TRUE(c.isEnabled());
    ASSERT_EQ(c.getEntity(), nullptr);

    c.setEnabled(false);
    ASSERT_EQ(c.isEnabled(), false);

    c.setEnabled(true);
    ASSERT_EQ(c.isEnabled(), true);
}
