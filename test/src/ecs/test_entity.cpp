#include "gtest/gtest.h"
#include "kine/ecs/entity.h"
#include "kine/ecs/component.h"
#include "kine/ecs/entity_manager.h"

namespace {
class TestComponent : public kine::ecs::Component {
public:
  TestComponent() {}
};
}

using namespace kine::ecs;

TEST(ecs_Entity, initial) {
  EntityManager em;
  auto e = em.createEntity();

  ASSERT_EQ(e->getName(), std::string());
  const auto& components = e->getComponents();
  ASSERT_EQ(components.size(), 0);

  ASSERT_EQ(e->getEntityManager(), &em);

  em.destroyEntity(e);
}

TEST(ecs_Entity, add_component) {
  EntityManager em;
  auto e = em.createEntity();

  TestComponent c;

  e->addComponent(&c);

  const auto& components = e->getComponents();
  ASSERT_EQ(components.size(), 1);
}

TEST(ecs_Entity, remove_component) {
  EntityManager em;
  auto e = em.createEntity();

  TestComponent c;
  e->addComponent(&c);

  const auto& components = e->getComponents();
  ASSERT_EQ(components.size(), 1);

  e->removeComponent(&c);
  ASSERT_EQ(components.size(), 0);
}
