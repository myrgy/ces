#include "gtest/gtest.h"
#include "gmock/gmock.h"

#include "kine/ecs/system.h"
#include "kine/ecs/system_manager.h"

namespace {
    class MockSystem : public kine::ecs::System<MockSystem> {
    public:
        MockSystem() : kine::ecs::System<MockSystem>("Mock System") {}

        MOCK_METHOD0(onBeforeStarted, void());
        MOCK_METHOD0(onAfterStarted,  void());
        MOCK_METHOD0(onBeforeStopped, void());
        MOCK_METHOD0(onAfterStopped,  void());
    };
}

TEST(ecs_SystemManager, systemManager) {
    kine::ecs::SystemManager sm;

    sm.start();
    sm.stop();
}

TEST(ecs_SystemManager, null_system) {
    kine::ecs::SystemManager sm;
    EXPECT_THROW(sm.addSystem(nullptr), std::runtime_error);
}

TEST(ecs_SystemManager, start_stop) {
    kine::ecs::SystemManager sm;
    MockSystem ms;
    sm.addSystem(&ms);

    EXPECT_CALL(ms, onBeforeStarted()).Times(1);
    EXPECT_CALL(ms, onAfterStarted()).Times(1);
    sm.start();

    ASSERT_EQ(ms.isStarted(), true);

    EXPECT_CALL(ms, onBeforeStopped()).Times(1);
    EXPECT_CALL(ms, onAfterStopped()).Times(1);
    sm.stop();
}

