#include "gtest/gtest.h"
#include "kine/ecs/system.h"
#include "kine/ecs/component.h"
#include "kine/unused.h"

TEST(ecs_System, InstatiationTest) {
  kine::ecs::BaseSystem sys("Test System");
  kine::unused(sys);

}

TEST(ecs_System, startTest) {
  kine::ecs::BaseSystem sys("Test System");
  sys.start();
  ASSERT_TRUE(sys.isStarted());

  sys.stop();
  ASSERT_FALSE(sys.isStarted());

  ASSERT_EQ(sys.getSystemManager(), nullptr);
}

namespace {

struct C1 : public kine::ecs::Component { };
struct C2 : public kine::ecs::Component { };
struct C3 : public kine::ecs::Component { };

struct S1 : public kine::ecs::System<S1>{
  friend class kine::ecs::System<S1>;

  S1() : kine::ecs::System<S1>("S1") { }

private:
  C1* createC1(int x) {
    return new C1();
  }

  void destroyC1(C1* c) {
    delete c;
  }

  C2* createC2() {
    return new C2();
  }

  void destroyC2(C2* c) {
    delete c;
  }

  C3* createC3() {
    return new C3();
  }

  void destroyC3(C3* c) {
    delete c;
  }

  using Components_t = kine::TypeValueList<
          KINE_DEFINE_COMPONENT(C1, S1::createC1, S1::destroyC1),
          KINE_DEFINE_COMPONENT(C2, S1::createC2, S1::destroyC2),
          KINE_DEFINE_COMPONENT(C3, S1::createC3, S1::destroyC3)
          >;
};
} // namespace

TEST(ecs_System, createComponent) {

  S1 s1;
  auto c1 = s1.create<C1>(42);
  auto c2 = s1.create<C2>();
  auto c3 = s1.create<C3>();

  ASSERT_NE(c1, nullptr);
  ASSERT_NE(c2, nullptr);
  ASSERT_NE(c3, nullptr);

  s1.destroy(c1);
  s1.destroy(c2);
  s1.destroy(c3);
}
