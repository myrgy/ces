#include "gtest/gtest.h"
#include "kine/scope.h"

TEST(Scope, testSuccess) {
    bool val = false;

    {
        SCOPE_SUCCESS {
            val = true;
        };
    }
    ASSERT_TRUE(val);
}

TEST(Scope, testFailed) {
    bool val = false;

    try {
        SCOPE_FAILED {
            val = true;
        };

        throw int(5);
    }
    catch (...) {

    }

    ASSERT_TRUE(val);
}

TEST(Scope, testExit) {
    bool val = false;
    {
        SCOPE_EXIT {
            val = true;
        };
    }

    ASSERT_TRUE(val);
}

TEST(Scope, testFailed2) 
{
  bool val = false;

  try
  {
    throw int(5);
    
    SCOPE_FAILED {
      val = true;
    };
  }
  catch(...)
  {
    
  }
  
  ASSERT_FALSE(val);
}
