#include "gtest/gtest.h"
#include "kine/math/math.h"

TEST(Math, sqrt) {
    const float v = 9.0f;
    const float sv = kine::math::sqrt(v);

    ASSERT_NEAR(3.0f, sv, 0.00001f);
}