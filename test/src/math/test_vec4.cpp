#include <sstream>

#include "gtest/gtest.h"
#include "kine/math/vec4.h"
#include "kine/math/math.h"
#include "kine/helpers.h"

using namespace kine::math;

namespace {
constexpr float eps = 0.00001f;

void AssertEq(const vec4& lho, const vec4& rho) {
  ASSERT_NEAR(lho.x, rho.x, eps);
  ASSERT_NEAR(lho.y, rho.y, eps);
  ASSERT_NEAR(lho.z, rho.z, eps);
  ASSERT_NEAR(lho.w, rho.w, eps);
}
}

TEST(Math, vec4_default_init) {
  kine::math::vec4 v;

  ASSERT_EQ(v.x, 0.0f);
  ASSERT_EQ(v.y, 0.0f);
  ASSERT_EQ(v.z, 0.0f);
  ASSERT_EQ(v.w, 0.0f);
}

TEST(Math, vec4_zero) {
  ASSERT_EQ(vec4::zero.x, 0.0f);
  ASSERT_EQ(vec4::zero.y, 0.0f);
  ASSERT_EQ(vec4::zero.z, 0.0f);
  ASSERT_EQ(vec4::zero.w, 0.0f);
}

TEST(Math, vec4_eq_1) {
  const auto v = vec4(1.0f, 2.0f, 3.3f, 0.0f);
  ASSERT_EQ(v, v);
}

TEST(Math, vec4_eq_2) {
  const auto v1 = vec4(1.0f, 2.0f, 3.3f);
  const auto v2 = vec4(1.0f, 2.0f, 3.3f);

  ASSERT_EQ(v1, v2);
  ASSERT_EQ(v2, v1);
}

TEST(Math, vec4_neq_1) {
  const auto v1 = vec4(1.0f, 0.0f, 0.0f);
  const auto v2 = vec4(2.0f, 0.0f, 0.0f);

  ASSERT_NE(v1, v2);
}

TEST(Math, vec4_neq_2) {
  const auto v1 = vec4(0.0f, 1.0f, 0.0f);
  const auto v2 = vec4(0.0f, 2.0f, 0.0f);

  ASSERT_NE(v1, v2);
}

TEST(Math, vec4_neq_3) {
  const auto v1 = vec4(0.0f, 0.0f, 1.0f);
  const auto v2 = vec4(0.0f, 0.0f, 2.0f);

  ASSERT_NE(v1, v2);
}

TEST(Math, vec4_plus) {
  const auto v1 = vec4(1.0f, 2.0f, 3.0f);
  const auto v2 = vec4(4.3f, 5.0f, 7.0f);

  const auto v = v1 + v2;
  const auto expected = vec4(5.3f, 7.0f, 10.f);

  AssertEq(v, expected);
}

TEST(Math, vec4_minus) {
  const auto v1 = vec4(1.0f, 2.0f, 3.0f);
  const auto v2 = vec4(4.3f, 5.0f, 7.0f);

  const auto v = v1 - v2;
  const auto expected = vec4(-3.3f, -3.0f, -4.0f);

  AssertEq(v, expected);
}

TEST(Math, vec4_squareMagnitude) {
  const auto v = vec4(1.0f, 2.0f, 3.0f);
  const auto sm = 1.0f + 4.0f + 9.0f;

  ASSERT_NEAR(v.squareMagnitude(), sm, eps);
}

TEST(Math, vec4_magnitude) {
  const auto v = vec4(1.0f, 2.0f, 3.0f);
  const auto m = sqrt( v.squareMagnitude() );

  ASSERT_NEAR(v.magnitude(), m, eps);
}

TEST(Math, vec4_stream_out) {
  std::stringstream ss;
  const auto v = vec4(1.0f, 2.0f, 3.0f);

  ss << v;
}
