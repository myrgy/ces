#include "gtest/gtest.h"
#include "kine/type_value_list.h"

namespace {

    template<class C, int val>
    struct Node {
        using value_t = decltype(val);
        using type_t = C;

        static constexpr value_t value = val;
    };

    using TypeValues = kine::TypeValueList<
        Node<char, 3>,
        Node<int, 5>,
        Node<unsigned, 42>>;
}

TEST(TypeValueList, get) {
    const auto char_val = TypeValues::get<char>();
    ASSERT_EQ(char_val, 3);

    const auto int_val = TypeValues::get<int>();
    ASSERT_EQ(int_val, 5);

    const auto unsigned_val = TypeValues::get<unsigned>();
    ASSERT_EQ(unsigned_val, 42);
}

TEST(TypeValueList, find) {
    using char_result = TypeValues::find<char>::result_t;
    static_assert(std::is_same<char_result::type_t, char>::value, "Find failed");

    using int_result = TypeValues::find<int>::result_t;
    static_assert(std::is_same<int_result::type_t, int>::value, "Find failed");

    using unsigned_result = TypeValues::find<unsigned>::result_t;
    static_assert(std::is_same<unsigned_result::type_t, unsigned>::value, "Find valied");

    using not_found = TypeValues::find<long>::result_t;
    static_assert(!std::is_same<not_found::type_t, long>::value, "Invalid find result. Value not exist in list.");
}