#include <chrono>
#include <thread>
#include <stdexcept>

#include "gtest/gtest.h"

#include "kine/unused.h"
#include "kine/systems/renderer/renderers_system.h"

TEST(RenderersSystem, createTest) {
    kine::systems::RenderersSystem rs;
    kine::unused(rs);
}

TEST(RenderersSystem, notInitialized) {
    kine::systems::RenderersSystem rs;
    kine::systems::WindowsSystem::WindowShared ws;
    EXPECT_THROW(rs.createRenderer(ws), std::runtime_error);
}

TEST(RenderersSystem, nullWindowPointer) {
    kine::systems::RenderersSystem rs;
    rs.start();
    kine::systems::WindowsSystem::WindowShared ws;
    EXPECT_THROW(rs.createRenderer(ws), std::runtime_error);
    rs.stop();
}

