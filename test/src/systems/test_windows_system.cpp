#include <chrono>
#include <thread>
#include <stdexcept>

#include "gtest/gtest.h"

#include "kine/unused.h"
#include "kine/systems/windows/windows_system.h"

TEST(WindowsSystem, createTest) {
    kine::systems::WindowsSystem ws;
    kine::unused(ws);
}

TEST(WindowsSystem, startStop) {
    kine::systems::WindowsSystem ws;

    ws.start();
    ASSERT_TRUE(ws.isStarted());
    ws.stop();
}

TEST(WindowsSystem, createWindow) {
    kine::systems::WindowsSystem ws;

    ws.start();
    ASSERT_TRUE(ws.isStarted());

    auto w = ws.create<kine::systems::Window>(640, 480, " test window");
    std::this_thread::sleep_for(std::chrono::milliseconds(1));
    w.reset();
    ws.stop();
}

TEST(WindowsSystem, notInitialized) {
    kine::systems::WindowsSystem ws;
    EXPECT_THROW(ws.create<kine::systems::Window>(640, 480, " test window"), std::runtime_error);
}
