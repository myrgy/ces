#include "gtest/gtest.h"
#include "kine/macros.h"

TEST(Macros, toString) {
    const char *str = KINE_STR(value);
    ASSERT_STREQ(str, "value");
}

TEST(Macros, stringConcat) {
    const auto x = KINE_STR(KINE_CONCAT(val_, 1));
    ASSERT_STREQ(x, "val_1");
}
