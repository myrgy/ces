* Entity contains Components.
Each component send messages to other components or 
can fetch parent for other component by type.

* Each component stored in component systems.
* Systems create/destroy/update components.

One system can mannage different types of components.

All renderer are peformed in a seprated thread.

In general we have several threads:
- input & window processing
- resource loading
- renderer


TODO:
- correct processing all events for windows (resize, close, ect.)
- add per-thread log streams with syncronization in main thread


windows-system -> Window
                    |
                 RenderContext
                    |
RenderersSystem -> Renderer(window)
                    |
                    |
                    |
                    |
Entitie
   |->RenderableComponent