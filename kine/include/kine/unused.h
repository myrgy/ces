#pragma once

namespace kine {
/**
 * Used to suppress compiler warnings about unused variables.
 * example: kine::unused(var1, var2, var3);
 */
template<typename ... Args>
void unused(const Args& ...) { }
} // namespace kine
