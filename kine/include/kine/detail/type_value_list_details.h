#pragma once

#include <type_traits>

namespace kine {
namespace type_value_list_detail {

struct Error_item_not_found {
  struct InvalidValueType {};
  using type_t = InvalidValueType;
};

template <class T, class Node, class ... Tail>
struct find_impl {
  using result_t = typename std::conditional<std::is_same<T, typename Node::type_t>::value,
                                             Node,
                                             typename std::conditional< sizeof ... (Tail) != 0,
                                                                        typename find_impl<T, Tail ...>::result_t,
                                                                        Error_item_not_found
                                                                        >::type
                                             >::type;
};

template <class T, class Node>
struct find_impl<T, Node> {
  using result_t = typename std::conditional<std::is_same<T, typename Node::type_t>::value,
                                             Node,
                                             Error_item_not_found
                                             >::type;
};

} // namespace type_value_list_detail
} // namespace kine
