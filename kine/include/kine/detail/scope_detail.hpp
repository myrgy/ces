#pragma once

#include <type_traits>
#include "kine/macros.h"

namespace kine {
namespace scope_detail {
int getUncaughtExceptionCount() noexcept;

class UncaughtExceptionsCounter {
public:
  UncaughtExceptionsCounter()
    : exceptionCount_(getUncaughtExceptionCount()) {}

  bool isNewUncaughtException() const noexcept {
    return getUncaughtExceptionCount() > exceptionCount_;
  }

private:
  int exceptionCount_ = 0;
};

template<typename FunctionType, bool executeOnException>
class ScopeGuardForNewException {
public:
  explicit ScopeGuardForNewException(const FunctionType& fn)
    : function_(fn) {}

  explicit ScopeGuardForNewException(FunctionType&& fn)
    : function_(std::move(fn)) {}

  ~ScopeGuardForNewException() noexcept(executeOnException) {
    if (executeOnException == ec_.isNewUncaughtException()) {
      function_();
    }
  }

private:
  FunctionType function_;
  const kine::scope_detail::UncaughtExceptionsCounter ec_;
};

enum class ScopeGuardOnFail {};

template<typename FunctionType>
ScopeGuardForNewException<typename std::decay<FunctionType>::type, true>
operator+(ScopeGuardOnFail, FunctionType&& fn) {
  using AliasT = ScopeGuardForNewException<typename std::decay<FunctionType>::type, true>;
  return AliasT(std::forward<FunctionType>(fn));
}

enum class ScopeGuardOnSuccess {};

template<typename FunctionType>
ScopeGuardForNewException<typename std::decay<FunctionType>::type, false>
operator+(ScopeGuardOnSuccess, FunctionType&& fn) {
  using AliasT = ScopeGuardForNewException<typename std::decay<FunctionType>::type, false>;
  return AliasT(std::forward<FunctionType>(fn));
}

//
template<typename FunctionType>
class ScopeGuardExit {
public:
  explicit ScopeGuardExit(const FunctionType& fn)
    : function_(fn) {}

  explicit ScopeGuardExit(FunctionType&& fn)
    : function_(std::move(fn)) {}

  ~ScopeGuardExit() {
    function_();
  }

private:
  FunctionType function_;
};

enum class ScopeGuardOnExit {};

template<typename FunctionType>
ScopeGuardExit<typename std::decay<FunctionType>::type>
operator+(ScopeGuardOnExit, FunctionType&& fn) {
  using AliasT = ScopeGuardExit<typename std::decay<FunctionType>::type>;
  return AliasT(std::forward<FunctionType>(fn));
}
} // namespace detail
} // namespace kine
