#pragma once

#include <ostream>

#include "math/vec4.h"

namespace kine {
namespace math {

std::ostream& operator<<(std::ostream& os, const kine::math::vec4& vec);

} // namespace math
} // namespace kine
