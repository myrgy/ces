#pragma once

#include "kine/math/vec4.h"
#include "kine/ecs/component.h"

namespace kine {
namespace components {

class Transform : public ecs::Component {
public:
  Transform();

  math::vec4 position;
};
}
}
