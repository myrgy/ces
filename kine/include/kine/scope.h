#pragma once

#include "kine/detail/scope_detail.hpp"

#define SCOPE_FAILED \
  const auto KINE_ANONYMOUS_VARIABLE(__kineScope) KINE_ATTRIBUTE_UNUSED = kine::scope_detail::ScopeGuardOnFail() + [&]

#define SCOPE_SUCCESS \
  const auto KINE_ANONYMOUS_VARIABLE(__kineScope) KINE_ATTRIBUTE_UNUSED = kine::scope_detail::ScopeGuardOnSuccess() + [&]

#define SCOPE_EXIT \
  const auto KINE_ANONYMOUS_VARIABLE(__kineScope) KINE_ATTRIBUTE_UNUSED = kine::scope_detail::ScopeGuardOnExit() + [&]
