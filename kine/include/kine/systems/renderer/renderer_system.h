#pragma once

#include <mutex>

#include "kine/ecs/system.h"

namespace kine {
namespace systems {

class RenderContext;
  
class RendererSystem : public ecs::System<RendererSystem> {
  friend class RenderersSystem;

public:
  using Base = ecs::System<RendererSystem>;

public:
  bool isValid() const;

private:
  RendererSystem(RenderContext* context);

  /**
   * return false if rendering was failed.
   * renderer will be removed from rendering queue
   */
  bool present();

private:
  RenderContext* context_;
  std::mutex mutex_;
};
} // namespace systems
} // namespace kine
