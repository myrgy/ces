#pragma once

#include <future>
#include <vector>

#include "kine/ecs/system.h"

namespace kine {
namespace systems {

class Window;
class RendererSystem;
/**
 * RenderersSystem manage all RendererSystem. 
 * RendererSystem is locked to some window context.
 */
class RenderersSystem : public ecs::System<RenderersSystem> {
public:
  using Base = ecs::System<RenderersSystem>;

  RenderersSystem();
  ~RenderersSystem();

  RendererSystem* createRendererSystem(Window* window);
  void destroyRendererSystem(RendererSystem* rendererSystem);
  
private:
  // System
  void onBeforeStarted() override;
  void onBeforeStopped() override;
  //
  static void renderingThread(RenderersSystem* renderersSystem);

private:
  std::vector<RendererSystem*> renderrers_;

  std::mutex mutex_;
  std::future<void> result_;
  std::atomic<bool> isLooping_;
};
} // namespace systems
} // namespace kine
