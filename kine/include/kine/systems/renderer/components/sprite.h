//
// Created by Alexander Dalshov.
//
#pragma once

#include "kine/math/vec4.h"
#include "kine/ecs/component.h"

namespace kine {
    namespace renderer {
        namespace components {

            class Sprite : public ecs::Component {
                friend class RendererSystem;

            public:
                Sprite();

            private:
                void render();

            private:
                math::vec4 position;
            };
        } // namespace components
    } // namespace renderer
} // namespace kine

