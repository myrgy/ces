#pragma once

#include "kine/ecs/system.h"

namespace kine {
namespace systems {

class LoopSystem : public kine::ecs::System<LoopSystem>, public kine::ecs::IUpdatableSystem {

public:
  using Base = ecs::System<LoopSystem>;

  LoopSystem();

private:
  void update() override;
};

} // namespace systems
} // namespace kine
