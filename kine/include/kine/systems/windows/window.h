#pragma once

#include <string>

#include "kine/ecs/component.h"

namespace kine {
namespace systems {

struct WindowPrivate;
class RenderContext;
class WindowDelegate {};

class Window : public ecs::Component {
  friend class WindowsSystem;

public:
  void setVisible(bool visible);
  bool isVisible() const;

  /**
   * try to close window and ask window delegate for that action.
   * is delegate approve closing or delegate is not set - then window will close.
   * After window closed it will destroy rendering context and all dependent components.
   * Also it will be removed from all entities.
   */
  void close();

  RenderContext* getRenderContext() const;

private:
  Window(int width, int height, const std::string& title);
  ~Window();

  bool shouldClose() const;

private:
  WindowPrivate* d_;
};
} // namespace systems
} // namespace kine
