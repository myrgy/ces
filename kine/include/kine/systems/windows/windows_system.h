#pragma once

#include <vector>

#include "kine/ecs/system.h"
#include "window.h"

namespace kine {
namespace systems {

class WindowsSystem : public ecs::System<WindowsSystem>,
  public ecs::IUpdatableSystem {
  friend class ecs::System<WindowsSystem>;

public:
  using Base = ecs::System<WindowsSystem>;

  WindowsSystem();
  ~WindowsSystem();

  Window* createWindow(int width, int height, const std::string& title);
  void destroyWindow(Window* window);
 
private:
  void onBeforeStarted() override;
  void onBeforeStopped() override;
  void onAfterStopped() override;

  void update() override;

  void poolEvents();

private:
  std::vector<Window*> windows_;
};
} // namespace systems
} // namespace kine
