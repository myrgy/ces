#pragma once

namespace kine {
namespace systems {

class RenderContext {
public:
  virtual bool isValid() const = 0;
  virtual bool isCurrent() const = 0;
  virtual void makeCurrent() = 0;
  virtual void detach() = 0;
  virtual void swapBuffers() = 0;
};

} // namespace systems
} // namespace kine
