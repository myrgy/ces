#pragma once

namespace kine {
namespace math {
/**
  * vec4 is used to represent vecotor and point in homogeneous coordinate 
  * representation.
  * Vector is encoded as (x,y,z, 0)
  * Point is encoded as (x,y,z, 1)
  */
class vec4 {
public:
  vec4() = default;
  vec4(vec4 &&) = default;
  vec4(float x, float y, float z, float w = 0.0f);

  vec4 operator + (const vec4& rho) const;
  vec4 operator - (const vec4& rho) const;
  vec4 operator * (float s) const;
  vec4 operator / (float s) const;

  bool operator == (const vec4& rho) const;
  bool operator != (const vec4& rho) const;

  float magnitude() const;
  float squareMagnitude() const;

public:
  float x = 0.0f;
  float y = 0.0f;
  float z = 0.0f;
  float w = 0.0f;

  static const vec4 zero;
};
} // namespace math
} // namespace kine
