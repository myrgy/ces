#pragma once

namespace kine {
namespace math {
class Quaternion {
public:
  float w;
  float x;
  float y;
  float z;
};
}     // namespace math
} // namespace kine
