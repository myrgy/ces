#pragma once

namespace kine {
namespace math {

float sqrt(float x);

} // namespace math
} // namespace kine
