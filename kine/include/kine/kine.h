#pragma once

/*! \mainpage kine Index Page
 *
 * kine is entity-compoment-system engine. Based only on few libraries:
 *  - stl (with C++11 support)
 *  - glfw - for hiding platform-dependent window processing
 *  - gtest & gmock for testing
 *
 *  I'm using waf for building and doxygen tags for documentation.
 *
 * */

namespace kine
{

} // namespace kine

