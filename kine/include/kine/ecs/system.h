#pragma once

#include <utility>      // std::forward
#include <type_traits>  // std::is_base_of
#include <string>

#include "kine/type_value_list.h"

namespace kine {
namespace ecs {

class SystemManager;

/** updatable systems are added to special loop in SystemManager.
 * For such systems update called every frame.
 */
class IUpdatableSystem {
  friend class SystemManager;
protected:
  virtual void update() = 0;
};

/// base class for all systems
class BaseSystem {
  friend class SystemManager;
public:
  BaseSystem(std::string&& systemName);
  virtual ~BaseSystem();

  bool isStarted() const;

  void start();
  void stop();

  SystemManager* getSystemManager();

  const std::string& getName();

protected:
  BaseSystem(const BaseSystem& ) = delete;
  void operator = (const BaseSystem&  ) = delete;

  virtual void onBeforeStarted() { }
  virtual void onAfterStarted() { }
  virtual void onBeforeStopped() { }
  virtual void onAfterStopped() { }

private:
  void setSystemManager(SystemManager* systemManager);

private:
  bool isStarted_ = false;
  SystemManager* systemManager_ = nullptr;
  std::string systemName_;
};

/**
 * Node value for list.
 * Store pair of Type and structure with system methods pointer
 * used to create/destroy object of given type
 */
template<class C,
	 typename createMethod_t, createMethod_t componentCreateMethod,
         typename destroyMethod_t, destroyMethod_t componentDestroyMethod>
struct ComponentNode {
  using type_t = C;
  static constexpr createMethod_t createMethod = componentCreateMethod;
  static constexpr destroyMethod_t destroyMethod = componentDestroyMethod;
};
 
/**
 * This macro is usefull to simplify component defenitions in system's classes
 */
#define KINE_DEFINE_COMPONENT(Type, createMethod, destroyMethod) \
  kine::ecs::ComponentNode<Type, decltype(&createMethod), &createMethod, decltype(&destroyMethod), &destroyMethod> 

/**
 * Main system class. Derive from it to achieve all template magic with component create/destroy.
 */
template <class Derived>
class System : public kine::ecs::BaseSystem {
  using Components_t = kine::TypeValueList<>;

public:
  System(std::string&& systemName)
    : kine::ecs::BaseSystem(std::move(systemName)) {
    static_assert(std::is_base_of<System<Derived>, Derived>::value, "Derived is not derived from System<Derived>");
  }

  template<class C, class ... Argv>
  C* create(Argv&& ... argv) {
    using result_t = typename Derived::Components_t::template find<C>::result_t;
    static_assert(std::is_same<C, typename result_t::type_t>::value, "Component type not found");
    const auto createMethod = result_t::createMethod;
    Derived* sys = static_cast<Derived*>(this);
    return (sys->*createMethod)(std::forward<Argv>(argv) ...);
  }

  template<class C>
  void destroy(C* component) {
    if (component == nullptr)
      throw std::runtime_error("Null pointer exception");
    using result_t = typename Derived::Components_t::template find<C>::result_t;
    static_assert(std::is_same<C, typename result_t::type_t>::value, "Component type not found");
    const auto destroyMethod = result_t::destroyMethod;
    Derived* sys = static_cast<Derived*>(this);
    return (sys->*destroyMethod)(component);
  }
};
} // namespace ecs
} // namespace kine
