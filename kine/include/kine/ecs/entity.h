#pragma once

#include <string>
#include <vector>

namespace kine {
namespace ecs {

class Component;
class EntityManager;
/**
 * Entity combine all components into one logical object.
 * Used to allow components interactions.
 */
class Entity {

public:
  const std::string& getName() const;

  void addComponent(Component* component);
  void removeComponent(Component* component);
  const std::vector <Component*>& getComponents();

  EntityManager* getEntityManager() const;

private:
  friend class EntityManager;
  explicit Entity(EntityManager* entity_manager);
  ~Entity();

private:
  std::string name_;
  std::vector <Component*> components_;
  EntityManager* const manager_;
};
} // namespace ecs
} // namespace kine
