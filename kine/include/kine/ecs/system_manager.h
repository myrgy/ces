#include <vector>
#include <map>

namespace kine {
namespace ecs {
  
class BaseSystem;
class IUpdatableSystem;
  
class SystemManager {
public:
  /**
   * all system register in system manager.
   * System manager run all update functions
   */
  void addSystem(BaseSystem* system, int priority = 0);
  void removeSystem(BaseSystem* system);
  
  void start();
  void stop();

  // run one update
  void update();
  // run update while exit not called
  void loop();
  // use exit to break loop
  void exit();

private:
  friend struct SystemByPointer;
  using SystemsMap = std::multimap<int, BaseSystem*>;
  using LoopList = std::vector<IUpdatableSystem*>;

  SystemsMap systems_;           // all systems list
  LoopList loop_;                // list of all systems, which must be updated in loop
  bool exit_ = false;            // if this flag is true - then loop will end execution
};
} // namespace ecs
} // namespace kine
