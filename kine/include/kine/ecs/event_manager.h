#pragma once

namespace kine {
namespace ecs {
/**
 * This class is used to send system wide messages for modules with week connection and dependencies.
 */
class EventManager {};
} // namespace ecs
} // namespace kine
