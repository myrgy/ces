#pragma once

#include <vector>

namespace kine {
namespace ecs {

class Entity;
/**
 * Manage all entities. Create/destroy
 * TODO: use pools instead of new for entities.
 */
class EntityManager {
public:
  Entity* createEntity();
  void destroyEntity(Entity* entity);
  
  const std::vector<Entity*>& getEntities();

private:
  std::vector<Entity*> entities_;
};
} // namespace ecs
} // namespace kine
