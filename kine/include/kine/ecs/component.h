#pragma once

namespace kine {
namespace ecs {
class Entity;

class Component {

public:
  void setEnabled(const bool enabled);
  bool isEnabled() const;

  Entity* getEntity() const;

protected:
  Component();
  Component(const Component& ) = delete;
  void operator = (const Component&) = delete;
  virtual ~Component();

  virtual void onBeforeEnabled(bool /*enabledOld*/, bool /*enabledNew*/) {}
  virtual void onAfterEnabled(bool /*enabledOld*/, bool /*enabledNew*/) {}

private:
  friend class Entity;
  
  void setEntity(Entity* entity);
  
private:
  bool isEnabled_ = true;
  Entity* entity_ = nullptr;
};
} // namespace ecs
} // namespace kine
