#pragma once

#include "kine/detail/type_value_list_details.h"

namespace kine {
/**
   usage:
   1. define node type

        template<class T, int data>
        struct Node {
            using value_t = V;
            using type_t = T;

            static constexpr value_t value = value_;
       };

   2. create list

        using list_t = TypeValueList<
            Node<char, 5>,
            Node<int, 42>
            >;

   3. extract payload
    auto x = list_t::get<int>();
 */

template <class ... Nodes>
struct TypeValueList {
  template <class T>
  struct find {
    using result_t = typename type_value_list_detail::find_impl<T, Nodes ...>::result_t;
  };

  template<class T>
  static constexpr auto get()->decltype(find<T>::result_t::value) {
    using result_t = typename find<T>::result_t;
    static_assert(std::is_same<T, typename result_t::type_t>::value, "Item not found");
    return result_t::value;
  }
};
} // namespace kine
