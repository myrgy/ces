#pragma once

#ifdef __clang__
#  define KINE_COMPILER_CLANG   1
#else
#  warning "Unknown compiler"
#endif

#ifndef KINE_CONCAT
#  define KINE_CONCAT_IMPL(a, b) a ## b
#  define KINE_CONCAT(a, b) KINE_CONCAT_IMPL(a, b)
#endif

#ifndef KINE_STR
#  define KINE_STR_IMPL(x) #x
#  define KINE_STR(x) KINE_STR_IMPL(x)
#endif

#ifndef KINE_ANONYMUS_VARIABLE
#  ifdef __COUNTER__
#    define KINE_ANONYMOUS_VARIABLE(name) KINE_CONCAT(name,__COUNTER__)
#  else
#    define KINE_ANONYMOUS_VARIABLE(name) KINE_CONCAT(name, __LINE__)
#  endif
#endif

#ifndef KINE_ATTRIBUTE_UNUSED
#  ifdef KINE_COMPILER_CLANG
#    define KINE_ATTRIBUTE_UNUSED  __attribute__((unused))
#  else
#    define KINE_ATTRIBUTE_UNUSED
#  endif
#endif
