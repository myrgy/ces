#pragma once

#include <ostream>

// TODO: create per-thread log entries

namespace kine {

namespace log {
enum class Level {
  Global,
  Trace,
  Warning,
  Debug,
  None           // disable all logging
};

void setLevel(Level level);
Level getLevel();

class LogStream;

/**
 * This class used to dump all user data
 */
class LogStream {
public:
  LogStream(const LogStream& other);
  LogStream(LogStream&& other);
  ~LogStream();

  template< typename T>
  LogStream& operator << (const T& val) {
    if (!enabled_)
      return *this;

    ostream_ << val;
    return *this;
  }

  static LogStream create(const char* file, unsigned line, Level level);
private:
  LogStream(const char* file, unsigned line, Level level, std::ostream& ostream);

private:
  const bool enabled_;
  Level level_ = Level::Global;
  std::ostream& ostream_;
};
}
} // namespace kine

#ifndef kGlobal
#   define kGlobal()   kine::log::LogStream::create(__FILE__, __LINE__, kine::log::Level::Global)
#   define kTrace()    kine::log::LogStream::create(__FILE__, __LINE__, kine::log::Level::Trace)
#   define kWarning()  kine::log::LogStream::create(__FILE__, __LINE__, kine::log::Level::Warning)
#   define kDebug()    kine::log::LogStream::create(__FILE__, __LINE__, kine::log::Level::Debug)
#endif
