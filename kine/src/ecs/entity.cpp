#include <assert.h>

#include "kine/ecs/entity.h"
#include "kine/ecs/component.h"

namespace kine {
namespace ecs {
Entity::Entity(EntityManager* entity_manager)
  : manager_(entity_manager) {
  assert(manager_ && "Null pointer. Entity Manager");
}

Entity::~Entity(){
  assert(!components_.empty() && "Components were not deleted. Might cause memory leak");
}

const std::string& Entity::getName() const {
  return name_;
}

void Entity::addComponent(Component* component) {
  if (!component) {
    throw std::runtime_error("Component is null. Failed to add.");
  }

  if (component->getEntity()) {
    throw std::runtime_error("Component is owned by other ebtity");
  }

  components_.push_back(component);
  component->setEntity(this);
}

void Entity::removeComponent(Component* component) {
  if (!component)
    throw std::runtime_error("Component is NULL. Failed to add.");

  if (component->getEntity() != this) {
    throw std::runtime_error("Component owned by other entity.");
  }

  auto pend = std::remove(components_.begin(), components_.end(), component);
  if (pend != components_.end()) {
    (*pend)->setEnabled(false);
    components_.erase(pend, components_.end());
  }
}

const std::vector <Component*>& Entity::getComponents() {
  return components_;
}

EntityManager* Entity::getEntityManager() const {
  return manager_;
}
} // namespace ecs
} // namespace kine
