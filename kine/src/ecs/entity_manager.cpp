#include "kine/ecs/entity_manager.h"
#include "kine/ecs/entity.h"

namespace kine {
namespace ecs {

Entity* EntityManager::createEntity() {
  auto entity = new Entity(this);
  entities_.push_back(entity);
  return entity;
}

void EntityManager::destroyEntity(Entity* entity) {
  if (!entity)
    throw std::runtime_error("Null pointer exception");

  if (entity->getEntityManager() != this)
    throw std::runtime_error("Entiti is owned by other Entity Manger");

  auto it = std::find(entities_.begin(), entities_.end(), entity);
  if (it == entities_.end())
    throw std::runtime_error("Invalid entity");

  auto last = entities_.end() - 1;
  if (it != last) { // remove not last element in vector
    std::swap(*it, *last);
    it = last;
  }

  entities_.erase(it);

  delete entity;
}

const std::vector<Entity*>& EntityManager::getEntities() {
  return entities_;
}
} // namespace ecs
} // namespace kine
