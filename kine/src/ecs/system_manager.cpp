#include <assert.h>

#include "kine/ecs/system.h"
#include "kine/ecs/system_manager.h"

#include "kine/log.h"

namespace kine {
namespace ecs {

struct SystemByPointer {
  explicit SystemByPointer(BaseSystem* system) : system(system) {}

  bool operator()(const SystemManager::SystemsMap::value_type& item) const {
    return system == item.second;
  }

public:
  BaseSystem* system;
};

void SystemManager::addSystem(BaseSystem* system, int priority /* = 0 */) {
  if (!system) {
    throw std::runtime_error("system pointer is null");
  }

  if (system->getSystemManager()) {
    throw std::runtime_error("system already have system manager");
  }

  assert((std::find_if(systems_.begin(), systems_.end(), SystemByPointer(system)) == systems_.end()) && "System is already added to SystemManager.");

  systems_.insert(SystemsMap::value_type(priority, system));
  system->setSystemManager(this);
}

void SystemManager::removeSystem(BaseSystem* system) {
  if (!system) {
    throw std::runtime_error("system pointer is null");
  }

  if (system->getSystemManager() != this) {
    throw std::runtime_error("System attached to another System Manager");
  }

  auto it = std::find_if(systems_.begin(), systems_.end(), SystemByPointer(system));
  if (it == systems_.end()) {
    throw std::runtime_error("System not registered in System Manager");
  }

  systems_.erase(it);

  if (auto updatable = dynamic_cast<IUpdatableSystem*>(system)) {
    auto itUpd = std::find(loop_.begin(), loop_.end(), updatable);
    if (itUpd != loop_.end()) {
      loop_.erase(itUpd);
    }
  }
}

void SystemManager::start() {
  for (auto& systemData : systems_) {
    auto system = systemData.second;
    kTrace() << "Start " << system->getName();
    system->start();

    if (auto updatable = dynamic_cast<IUpdatableSystem*>(system)) {
      loop_.push_back(updatable);
    }
  }
}

void SystemManager::stop() {
  for (auto& systemData : systems_) {
    kTrace() << "Stop " << systemData.second->getName();
    systemData.second->stop();
  }
  loop_.clear();
}

void SystemManager::update() {
  for (auto system : loop_) {
    system->update();
  }
}

void SystemManager::loop() {
  while (!exit_) {
    update();
  }
}

void SystemManager::exit() {
  exit_ = true;
}
} // namespace ecs
} // namespace kine
