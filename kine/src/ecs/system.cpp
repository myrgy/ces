#include <stdexcept>
#include "kine/ecs/system.h"

namespace kine {
    namespace ecs {

        BaseSystem::BaseSystem(std::string && systemName)
                : systemName_(std::move(systemName))
        {
        }

        BaseSystem::~BaseSystem() {
        }

        bool BaseSystem::isStarted() const {
            return isStarted_;
        }

        void BaseSystem::start() {
            onBeforeStarted();
            isStarted_ = true;
            onAfterStarted();
        }

        void BaseSystem::stop() {
            onBeforeStopped();
            isStarted_ = false;
            onAfterStopped();
        }

        SystemManager * BaseSystem::getSystemManager() {
            return systemManager_;
        }

        const std::string & BaseSystem::getName() {
            return systemName_;
        }

        void BaseSystem::setSystemManager(SystemManager *systemManager) {
            if (!systemManager)
                throw std::runtime_error("System manager is nullptr.");

            if (systemManager_)
                throw std::runtime_error("System Manager already set.");

            systemManager_ = systemManager;
        }
    } // namespace ecs
} // namespace kine
