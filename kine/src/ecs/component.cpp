#include "kine/ecs/component.h"

namespace kine {
    namespace ecs {

        Component::Component() {
        }

        Component::~Component() {
        }

        void Component::setEnabled(const bool enabled) {
            if (isEnabled_ == enabled)
                return;

            onBeforeEnabled(isEnabled_, isEnabled_);
            isEnabled_ = enabled;
            onAfterEnabled(isEnabled_, isEnabled_);
        }

        bool Component::isEnabled() const {
            return isEnabled_;
        }

        void Component::setEntity(Entity * entity) {
            entity_ = entity;
        }

        Entity * Component::getEntity() const {
            return entity_;
        }
    } // namespace ecs
} // namespace kine
