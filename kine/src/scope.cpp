#include "kine/scope.h"

#if  defined(KINE_COMPILER_CLANG) && (KINE_COMPILER_CLANG == 1)
// clang
namespace __cxxabiv1 {
    // Each thread in a C++ program has access to a __cxa_eh_globals object.
    struct __cxa_eh_globals {
        void *caughtExceptions;
        unsigned int uncaughtExceptions;
    };

    extern "C" __cxa_eh_globals* __cxa_get_globals() noexcept;
}

namespace kine {
    namespace scope_detail {
        int getUncaughtExceptionCount() noexcept {
            return __cxxabiv1::__cxa_get_globals()->uncaughtExceptions;
        }
    }
}
#else
#   error "Unknow compiler. Scope is unsupported"
#endif
