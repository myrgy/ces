#include <GLFW/glfw3.h>
#include <stdexcept>
#include <assert.h>

#include "kine/ecs/system_manager.h"
#include "kine/systems/windows/windows_system.h"
#include "kine/log.h"

namespace kine {
namespace systems {

WindowsSystem::WindowsSystem()
  : Base("Windows System")
{}

WindowsSystem::~WindowsSystem() {
  assert(windows_.empty() && "All windows must be deleted before system destroy.");
}

Window* WindowsSystem::createWindow(int width, int height, const std::string& title) {

  kTrace() << "WindowsSystem::createWindow(...)";

  if (!isStarted()) {
    throw std::runtime_error("System not initialized");
  }

  auto window = new Window(width, height, title);
  windows_.push_back(window);
  return window;
}

void WindowsSystem::destroyWindow(Window* window) {
  if (!window)
    throw std::runtime_error("Window is null pointer");
  auto it = std::find(windows_.begin(), windows_.end(), window);
  if (it == windows_.end())
    throw std::runtime_error("Window is not found in WinowsSystem");
  delete *it;
  windows_.erase(it);
}

void WindowsSystem::poolEvents() {
  glfwPollEvents();

  for (auto& w : windows_) {
    if (w->shouldClose() && w->isVisible()) {
      kTrace() << "WindowsSystem::poolEvents() hide window because of shouldClose";
      w->setVisible(false);
    }
  }
}

void WindowsSystem::onBeforeStarted() {
  kTrace() << "WindowsSystem::onBeforeStarted()";
  if (!glfwInit()) {
    throw std::runtime_error("Failed to initialize glfw");
  }
}

void WindowsSystem::onBeforeStopped() {
  kTrace() << "WindowsSystem::onBeforeStopped()";
  // for(auto& w : windows_) {
    // if (!w.unique())
    //   throw std::runtime_error("window poiter used by other systems.");
    // w.reset();
  // }
  // windows_.clear();
}

void WindowsSystem::onAfterStopped() {
  kTrace() << "WindowsSystem::onAfterStopped()";
  glfwTerminate();
}

void WindowsSystem::update() {
  poolEvents();
}
} // namespace systems
} // namespace kine
