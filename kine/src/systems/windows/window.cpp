#include "kine/systems/windows/window.h"
#include "kine/systems/windows/render_context.h"
#include "kine/log.h"

#include <list>
#include <GLFW/glfw3.h>

namespace kine {
namespace systems {

namespace {

class GlfwRenderContext : public RenderContext {
public:
  GlfwRenderContext(GLFWwindow* window)
    : window_(window) {}

  bool isValid() const override {
    return window_ != nullptr;
  }

  bool isCurrent() const override {
    return window_ && window_ == glfwGetCurrentContext();
  }

  void makeCurrent() override {
    glfwMakeContextCurrent(window_);
  }

  void detach() override {
    if (isCurrent()) {
      glfwMakeContextCurrent(nullptr);
    }
  }

  void swapBuffers() override {
    if (window_)
      glfwSwapBuffers(window_);
  }

  void invalidate() {
    detach();
    window_ = nullptr;
  }

private:
  GLFWwindow* window_;
};
} // namespace

struct WindowPrivate {
  GLFWwindow* window = nullptr;
  std::unique_ptr<GlfwRenderContext> context;
};

Window::Window(int width, int height, const std::string& title)
  : d_(new WindowPrivate) {
  d_->window = glfwCreateWindow(width, height, title.c_str(), NULL, NULL);
  if (!d_->window) {
    throw std::runtime_error("Failed to create window");
  }
  glfwSetWindowUserPointer(d_->window, this);
  glfwSetWindowCloseCallback(d_->window, [](GLFWwindow* win) {
                               kTrace() << "OnGLFWWindowClose";
                               auto winPtr = reinterpret_cast<kine::systems::Window*>(glfwGetWindowUserPointer(win));
                               winPtr->close();
                             });
}

Window::~Window() {
  kTrace() << "Window::~Window()";
  if (d_->window) {
    if (d_->context) {
      kTrace() << "invalidate window render context";
      d_->context->invalidate();
    }
    glfwDestroyWindow(d_->window);
    d_->window = nullptr;
  }

  delete d_;
}

void Window::setVisible(bool visible) {
  if (d_->window) {
    visible ? glfwShowWindow(d_->window) : glfwHideWindow(d_->window);
  }
}

bool Window::isVisible() const {
  bool visible = false;
  if (d_->window) {
    visible = glfwGetWindowAttrib(d_->window, GLFW_VISIBLE) != 0;
  }
  return visible;
}

void Window::close() {
  glfwSetWindowShouldClose(d_->window, 1);
}

RenderContext* Window::getRenderContext() const {
  if (d_->window && !d_->context) {
    d_->context.reset(new GlfwRenderContext(d_->window));
  }

  return d_->context.get();
}

bool Window::shouldClose() const {
  return glfwWindowShouldClose(d_->window) != 0;
}
} // namespace systems
} // namespace kine
