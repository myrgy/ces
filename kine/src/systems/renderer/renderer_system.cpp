#include <thread>

#include "kine/systems/renderer/renderer_system.h"
#include "kine/systems/windows/render_context.h"
#include "kine/log.h"
#include "kine/unused.h"

namespace kine {
namespace systems {

RendererSystem::RendererSystem(RenderContext* context)
  : Base("Renderer System")
  , context_(context) {
  if (!context_)
    throw std::runtime_error("Invalid context");
}

bool RendererSystem::isValid() const {
  return context_ && context_->isValid();
}

bool RendererSystem::present() {
  std::lock_guard<std::mutex> lock(mutex_);
  kine::unused(lock);

  if (!context_->isValid())
    return false;

  std::this_thread::sleep_for(std::chrono::milliseconds(1000));
  kTrace() << "present";

  return true;
}
} // namespace systems
} // namespace kine
