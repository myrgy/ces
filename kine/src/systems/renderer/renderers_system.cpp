#include <assert.h>

#include "kine/systems/renderer/renderers_system.h"
#include "kine/systems/renderer/renderer_system.h"
#include "kine/systems/windows/render_context.h"
#include "kine/systems/windows/window.h"
#include "kine/log.h"
#include "kine/unused.h"
#include "kine/ecs/system_manager.h"

namespace kine {
namespace systems {

RenderersSystem::RenderersSystem()
  : Base("Renderers System")
{}

RenderersSystem::~RenderersSystem() {
  assert(renderrers_.empty() && "Renderers were not destroyed before system destroy. Memory leak.");
}

RendererSystem* RenderersSystem::createRendererSystem(Window* window) {
  if (!isStarted()) {
    throw std::runtime_error("System not initialized");
  }

  if (!window) {
    throw std::runtime_error("Null window pointer");
  }
  kTrace() << "create renderer system";
  auto rendererSystem = new RendererSystem(window->getRenderContext());

  std::lock_guard<std::mutex> lock(mutex_);
  kine::unused(lock);

  kTrace() << "add to rernderers list";
  renderrers_.push_back(rendererSystem);

  auto systemManager = getSystemManager();
  if (systemManager) {
    kTrace() << "register in system manager";
    systemManager->addSystem(rendererSystem);
  }
  return rendererSystem;
}

void RenderersSystem::destroyRendererSystem(RendererSystem* rendererSystem) {
  if (!rendererSystem)
    throw std::runtime_error("Renderer is null pointer");
  auto it = std::find(renderrers_.begin(), renderrers_.end(), rendererSystem);
  if (it == renderrers_.end())
    throw std::runtime_error("Renderer not found in this renderesSystem");

  auto systemManager = getSystemManager();
  if (systemManager) {
    kTrace() << "unregister in system manager";
    systemManager->removeSystem(rendererSystem);
  }
  
  std::lock_guard<std::mutex> lock(mutex_);
  kine::unused(lock);
  delete *it;
  renderrers_.erase(it);
}
  
void RenderersSystem::onBeforeStarted() {
  kTrace() << "RenderersSystem::onBeforeStarted()";
  isLooping_.store(true);
  result_ = std::async(std::launch::async, RenderersSystem::renderingThread, this);
}

void RenderersSystem::onBeforeStopped() {
  kTrace() << "RenderersSystem::onBeforeStopped()";
  isLooping_.store(false);
  result_.wait();
  result_.get();           // if some exception - rethrow in current thread

  std::lock_guard<std::mutex> lock(mutex_);
  kine::unused(lock);
  // for(auto& r : renderrers_) {
  //   if (!r.unique())
  //     throw std::runtime_error("window poiter used by other systems.");
  //   r.reset();
  // }

  kTrace() << "RenderersSystem::onBeforeStopped() stopped.";
}

void RenderersSystem::renderingThread(RenderersSystem* renderersSystem) {
  kTrace() << "RenderersSystem::renderingThread() begin.";

  while(renderersSystem->isLooping_.load()) {
    std::lock_guard<std::mutex> lock(renderersSystem->mutex_);
    kine::unused(lock);

    for(auto& r : renderersSystem->renderrers_) {
      auto res = r->present();
      //TODO: process render result
      kine::unused(res);
    }
  }

  kTrace() << "RenderersSystem::renderingThread() end.";
}
} // namespace systems
} // namespace kine
