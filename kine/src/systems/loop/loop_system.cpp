#include "kine/systems/loop/loop_system.h"

namespace kine {
namespace systems {

LoopSystem::LoopSystem()
  : Base ("Loop System")
{}

void LoopSystem::update() {}
} // namespace systems
} // namespace kine
