#include <iostream>
#include <sstream>
#include <iomanip>
#include <thread>

#include "kine/log.h"
#include "kine/unused.h"

namespace kine {

    namespace log {

        namespace {
            const std::chrono::steady_clock::time_point g_startTime = std::chrono::steady_clock::now();
            Level g_logLevel = Level::Global;

            const std::string & toString(Level level) {

                static const auto global  = std::string("Global");
                static const auto trace   = std::string("Trace");
                static const auto warning = std::string("Warning");
                static const auto debug   = std::string("Debug");
                static const auto empty   = std::string();

                switch(level){
                    case Level::Global:  return global;
                    case Level::Trace:   return trace;
                    case Level::Warning: return warning;
                    case Level::Debug:   return debug;
                    default:
                        return empty;
                }
                return empty;
            }

            std::recursive_mutex streamLock;

            std::ostream& getLogStream() {
                return std::cout;
            }

            std::string toString(const std::chrono::steady_clock::time_point& tp) {
                auto duration = tp - g_startTime;

                const auto hours = std::chrono::duration_cast<std::chrono::hours>(duration).count();
                duration -= std::chrono::hours(hours);

                const auto minutes = std::chrono::duration_cast<std::chrono::minutes>(duration).count();
                duration -= std::chrono::minutes(minutes);

                const auto seconds = std::chrono::duration_cast<std::chrono::seconds>(duration).count();
                duration -= std::chrono::seconds(seconds);

                const auto milliseconds = std::chrono::duration_cast<std::chrono::milliseconds>(duration).count();

                std::stringstream str;
                str<<std::setfill('0')<<std::setw(2)<<hours<<":"
                        <<std::setfill('0')<<std::setw(2)<<minutes<<":"
                        <<std::setfill('0')<<std::setw(2)<<seconds<<'.'
                        <<std::setfill('0')<<std::setw(3)<<milliseconds;
                return str.str();
            }
        }

        //
        void setLevel(Level level) {
            g_logLevel = level;
        }

        Level getLevel() {
            return g_logLevel;
        }

        //
        LogStream LogStream::create(const char* file, unsigned line, Level level) {
            auto & stream = getLogStream();
            return LogStream(file, line, level, stream);
        }

        LogStream::LogStream(const char* file, unsigned line, Level level, std::ostream& ostream) :
            enabled_ (level <= getLevel()),
            level_(level),
            ostream_(ostream) {
	    
		kine::unused(file, line);

            if (enabled_) {
                streamLock.lock();
                auto now = std::chrono::steady_clock::now();
                ostream_ << toString(now) << " [" << toString(level) << ":" << std::this_thread::get_id() << "] ";
            }
        }

        LogStream::LogStream(const LogStream& other) :
            enabled_(other.enabled_),
            level_(other.level_),
            ostream_(other.ostream_) {
        }

        LogStream::LogStream(LogStream&& other) :
            enabled_(other.enabled_),
            level_(other.level_),
            ostream_(other.ostream_) {
        }

        LogStream::~LogStream() {
            if (enabled_) {
                ostream_ << '\n';
                streamLock.unlock();
            }
        }
    } //namespace log
} // namespace kine

