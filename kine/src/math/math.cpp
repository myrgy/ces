#include "kine/math/math.h"
#include <cmath>

namespace kine {
namespace math {

float sqrt(float x) {
  return std::sqrt(x);
}

} // namespace math
} // namespace kine
