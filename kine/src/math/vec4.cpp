#include "kine/math/vec4.h"
#include "kine/math/math.h"

namespace kine {
namespace math {

const vec4 vec4::zero = vec4();

vec4::vec4(float x, float y, float z, float w) : x(x), y(y), z(z), w(w) {}

vec4 vec4::operator + (const vec4& rho) const {
  return vec4(x + rho.x, y + rho.y, z + rho.z, w + rho.w);
}

vec4 vec4::operator - (const vec4& rho) const {
  return vec4(x - rho.x, y - rho.y, z - rho.z, w - rho.w);
}

vec4 vec4::operator * (float s) const {
  return vec4(x * s, y * s, z * s, w * s);
}

vec4 vec4::operator / (float s) const {
  const auto inv_s = 1.f / s;
  return vec4(x * inv_s, y * inv_s, z * inv_s, w * inv_s);
}

bool vec4::operator == (const vec4& rho) const {
  return x == rho.x && y == rho.y && z == rho.z && w == rho.w;
}

bool vec4::operator!=(const vec4& rho) const {
  return x != rho.x || y != rho.y || z != rho.z || w != rho.w;
}

float vec4::squareMagnitude() const {
  const auto squareMagnitude = x * x + y * y + z * z + w * w;
  return squareMagnitude;
}

float vec4::magnitude() const {
  const auto magnitude = sqrt(x * x + y * y + z * z + w * w);
  return magnitude;
}

} // namespace math
} // namespace kine
