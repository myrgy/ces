#include "kine/helpers.h"

namespace kine {
namespace math {
std::ostream& operator<<(std::ostream& os, const kine::math::vec4& vec) {
  os << "Vec4(" << vec.x << ", " << vec.y << ", " << vec.z << ", " << vec.w << ")";
  return os;
}
}
}
