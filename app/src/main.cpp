#include <iostream> // sync_with_stdio

#include "kine/ecs/entity.h"
#include "kine/ecs/entity_manager.h"
#include "kine/ecs/system_manager.h"
#include "kine/log.h"
// systems
#include "kine/systems/renderer/renderers_system.h"
#include "kine/systems/windows/windows_system.h"

#include "kine/components/transform.h"

#include "kine/systems/renderer/components/sprite.h"

int main(int /*argc*/, char**/*argv*/) {
  std::ios_base::sync_with_stdio(false);   // don't use printf
  try {
    kine::log::setLevel(kine::log::Level::Warning);

    kine::ecs::SystemManager sm {};
    kine::ecs::EntityManager em {};

    auto app = em.createEntity();
    auto windowsSystem = new kine::systems::WindowsSystem();
    auto renderersSystem = new kine::systems::RenderersSystem();

    sm.addSystem(windowsSystem);
    sm.addSystem(renderersSystem);

    sm.start();

    // create windows
    auto mainWindow = windowsSystem->createWindow(640, 480, "Application");
    app->addComponent(mainWindow);

    auto debugWindow = windowsSystem->createWindow(100, 400, "Debug");
    app->addComponent(debugWindow);
    
    auto renderer = renderersSystem->createRendererSystem(mainWindow);
    {
      //auto t = em.createEntity();
      //      t->addComponent(std::make_shared<kine::components::Transform>());
      //auto sprite = renderer->create<kine::renderer::components::Sprite>();
      //t->addComponent(sprite);
    }

    kDebug() << "Start application loop";
    sm.loop();

    kDebug() << "Stop all systems";
    sm.stop();

    app->removeComponent(mainWindow);
    app->removeComponent(debugWindow);
    renderersSystem->destroyRendererSystem(renderer);

    windowsSystem->destroyWindow(debugWindow);
    windowsSystem->destroyWindow(mainWindow);

    sm.removeSystem(renderersSystem);
    sm.removeSystem(windowsSystem);

    delete renderersSystem;
    delete windowsSystem;
  }
  catch(std::runtime_error& ex) {
    kGlobal() << "Runtime exception: " << ex.what();
    return 1;
  }
  catch(...) {
    kGlobal() << "Unknown exception.";
    return 2;
  }
  return 0;
}
